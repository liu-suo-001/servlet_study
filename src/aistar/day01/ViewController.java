package aistar.day01;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/day01/form")
public class ViewController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("<!doctype html>");
        out.println("<html>");
        out.println("   <head>");
        out.println("       <meta charset='utf-8'>");
        out.println("       <title>用户登录</title>");
        out.println("   </head>");
        out.println("   <body>");
        //method默认是get方式
        //action指向的是后端的地址 - 项目上下文路径/映射路径
        out.println("   <form action='/j01/day01/action' method='post'>");
        out.println("       <input type='text' name='username'>");
        out.println("       <input type='password' name='password'>");
        out.println("       <input type='submit' value='login'>");
        out.println("   </form>");

        out.println("<a href='/j01/day01/action?username=java'>get请求</a>");

        out.println("   </body>");
        out.println("</html>");
    }
}
