package aistar.day04;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/session/create")
public class sessionCreate extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String remark = req.getParameter("remark");
        HttpSession hsp = req.getSession();
        hsp.setAttribute("remark",remark);

        resp.sendRedirect("/j01/session/get");
    }
}
