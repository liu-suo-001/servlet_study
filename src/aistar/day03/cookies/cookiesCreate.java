package aistar.day03.cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/cookies/create")
public class cookiesCreate extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = "星星";
        name = URLEncoder.encode(name,"utf-8");
        name = URLEncoder.encode(name,"utf-8");
        Cookie c = new Cookie("username",name);
        c.setPath("/");
        c.setMaxAge(60*10);
        resp.addCookie(c);
    }
}
