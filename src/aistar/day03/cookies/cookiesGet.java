package aistar.day03.cookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

@WebServlet("/cookies/get")
public class cookiesGet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        if(null!=cookies && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                if("username".equals(cookies[i].getName())){
                    String value  = cookies[i].getValue();
                    value = URLDecoder.decode(value,"utf-8");
                    value = URLDecoder.decode(value,"utf-8");
                    System.out.println(cookies[i].getName()+":"+value);
                }
            }
        }
    }
}
