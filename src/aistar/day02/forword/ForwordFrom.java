package aistar.day02.forword;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/forword/from")
public class ForwordFrom extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("你能看见不");

        req.setAttribute("name","admin");

        System.out.println("redirect..from...");
        req.getRequestDispatcher("/forword/to").forward(req,resp);
    }
}
