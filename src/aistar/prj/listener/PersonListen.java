package aistar.prj.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class PersonListen implements HttpSessionListener{
    private int num = 0;
    ServletContext application;
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        num++;
        application.setAttribute("person",num);
        System.out.println(application.getAttribute("person"));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        if(num>0){
            num--;
            application.setAttribute("person",num);
        }

    }


}
