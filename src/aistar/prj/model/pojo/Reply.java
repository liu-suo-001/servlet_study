package aistar.prj.model.pojo;

import java.io.Serializable;

public class Reply implements Serializable {
    private int id;
    private int commentId;
    private String content;
    private int userId;
    private int replyId;

    public Reply() {
    }

    public Reply(int id, int commentId, String content, int userId, int replyId) {
        this.id = id;
        this.commentId = commentId;
        this.content = content;
        this.userId = userId;
        this.replyId = replyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Reply{");
        sb.append("id=").append(id);
        sb.append(", commentId=").append(commentId);
        sb.append(", content='").append(content).append('\'');
        sb.append(", userId=").append(userId);
        sb.append(", replyId=").append(replyId);
        sb.append('}');
        return sb.toString();
    }
}
