package aistar.prj.model.pojo;

import java.io.Serializable;
import java.util.Date;

public class Video implements Serializable {
    private int id;
    private String title;
    private String videoUrl;
    private int userId;
    private Date createDate;
    private int likes;
    private double score;
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Video() {
    }

    public Video(int id, String title, String videoUrl, int userId, Date createDate) {
        this.id = id;
        this.title = title;
        this.videoUrl = videoUrl;
        this.userId = userId;
        this.createDate = createDate;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Video{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", videoUrl='").append(videoUrl).append('\'');
        sb.append(", userId=").append(userId);
        sb.append(", createDate=").append(createDate);
        sb.append(", likes=").append(likes);
        sb.append(", score=").append(score);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
