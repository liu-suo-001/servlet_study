package aistar.prj.model.pojo;

import java.io.Serializable;

public class Comment implements Serializable {
    private int id;
    private String content;
    private int userId;
    private int videoId;
    private  int likes;
    private int commentsId;

    public Comment() {
    }

    public Comment(int id, String content, int userId, int videoId, int likes, int commentsId) {
        this.id = id;
        this.content = content;
        this.userId = userId;
        this.videoId = videoId;
        this.likes = likes;
        this.commentsId = commentsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(int commentsId) {
        this.commentsId = commentsId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Comment{");
        sb.append("id=").append(id);
        sb.append(", content='").append(content).append('\'');
        sb.append(", userId=").append(userId);
        sb.append(", videoId=").append(videoId);
        sb.append(", likes=").append(likes);
        sb.append(", commentsId=").append(commentsId);
        sb.append('}');
        return sb.toString();
    }
}
