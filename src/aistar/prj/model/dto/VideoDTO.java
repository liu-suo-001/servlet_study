package aistar.prj.model.dto;

import aistar.prj.model.pojo.Video;

public class VideoDTO{
    private Video video;

    private String username;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("VideoDTO{");
        sb.append("video=").append(video);
        sb.append(", username='").append(username).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

