package aistar.prj.controller;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.pojo.Video;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/video/my/view")
public class MyRelese extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Video> list = (List<Video>) req.getAttribute("videos");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
        out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
        out.println("<title>bootstrap是一款响应式的UI[User-Interface]框架</title>");
        out.println("<!-- 引入bootstrap.css - 开发环境/bootstrap.min.css - 生产环境-->");
        out.println("<link rel='stylesheet' href='/j01/plugins/bootstrap-3.4.1-dist/css/bootstrap.css'>");
        out.println("<link rel='stylesheet' href='/j01/css/user/login.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<ul id='myTab' class='nav nav-tabs'>");
        out.println("<li class='active'>");
        out.println("<a href='#home' data-toggle='tab'>全部视频");out.println("</a>");
        out.println("</li>");
        out.println("<li>");out.println("<a href='#ios' data-toggle='tab'>已发布视频");out.println("</a>");out.println("</li>");
        out.println("<li>");out.println("<a href='#none1' data-toggle='tab'>未发布视频");out.println("</a>");out.println("</li>");
        out.println("</ul>");
        out.println("<div id='myTabContent' class='tab-content'>");
        out.println("<div class='tab-pane fade in active' id='home'>");

        out.println("<div class='row'>");
        for (int i = 0; i < list.size(); i++) {
            out.println("<div class='col-sm-6 col-md-3'>");
            out.println("<div class='thumbnail' style='height:420px'>");
            if(list.get(i).getStatus()==0){
                out.println("<a href='/j01/myVideo/update?videoUpdate="+list.get(i).getId()+"'><img style='height:270px;'  src='/j01/imgs/"+list.get(i).getVideoUrl()+"'></a>");
            }else{
                out.println("<img style='height:270px;' src='/j01/imgs/"+list.get(i).getVideoUrl()+"'>");
            }

            out.println("<div class='caption'>");
            out.println("<h3 class='text-center text-info'>"+list.get(i).getTitle()+"");out.println("</h3>");
            out.println("<p class='text-center text-succes'>点赞量: "+list.get(i).getLikes()+"");out.println("</p>");
            out.println("<p  class='text-center text-success'>评分:<span class='text-danger'>"+list.get(i).getScore()+"</span>");
            if(list.get(i).getStatus() == 2){
                out.println("<a href='/j01/myVideo/personalView?videoIdAdd="+list.get(i).getId()+"' class='btn btn-primary' role='button'>发布");
                out.println("</a>");
                out.println("<a href='/j01/myVideo/personalView?videoId="+list.get(i).getId()+"' class='btn btn-default' role='button'>删除");
                out.println("</a>");
            };
            out.println("</p>");


            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
        }
        out.println("</div>");


        out.println("</div>");
        out.println("<div class='tab-pane fade' id='ios'>");

        out.println("<div class='row'>");
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getStatus() == 0){
                out.println("<div class='col-sm-6 col-md-3'>");
                out.println("<div class='thumbnail' style='height:420px'>");
                out.println("<a href='/j01/myVideo/update?videoUpdate="+list.get(i).getId()+"'><img style='height:270px;'  src='/j01/imgs/"+list.get(i).getVideoUrl()+"'></a>");
                out.println("<div class='caption'>");
                out.println("<h3 class='text-center text-info'>"+list.get(i).getTitle()+"");out.println("</h3>");
                out.println("<p class='text-center text-succes'>点赞量: "+list.get(i).getLikes()+"");out.println("</p>");
                out.println("<p  class='text-center text-success'>评分:<span class='text-danger'>"+list.get(i).getScore()+"</span>");
                out.println("</p>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
            }

        }
        out.println("</div>");


        out.println("</div>");
        out.println("<div class='tab-pane fade' id='none1'>");

        out.println("<div class='row'>");
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getStatus()==2) {
                out.println("<div class='col-sm-6 col-md-3'>");
                out.println("<div class='thumbnail' style='height:420px'>");
                out.println("<img style='height:270px;' src='/j01/imgs/" + list.get(i).getVideoUrl() + "'>");
                out.println("<div class='caption'>");
                out.println("<h3 class='text-center text-info'>" + list.get(i).getTitle() + "");
                out.println("</h3>");
                out.println("<p class='text-center text-succes'>点赞量: " + list.get(i).getLikes() + "");
                out.println("</p>");
                out.println("<p  class='text-center text-success'>评分:<span class='text-danger'>" + list.get(i).getScore() + "</span>");
                    out.println("<a href='/j01/myVideo/personalView?videoIdAdd=" + list.get(i).getId() + "' class='btn btn-primary' role='button'>发布");
                    out.println("</a>");
                    out.println("<a href='/j01/myVideo/personalView?videoId=" + list.get(i).getId() + "' class='btn btn-default' role='button'>删除");
                    out.println("</a>");
                out.println("</p>");


                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
            }
        }
        out.println("</div>");




        out.println("</div>");
        out.println("</div>");





        out.println("<!-- 引入jquery框架 -->");
        out.println("<script src='/j01/plugins/jquery.min.js'>");
        out.println("</script>");
        out.println("<!-- 引入bootstrap.js/bootstrap.min.js,bootstrap依赖于jquery框架 -->");
        out.println("<script src='/j01/plugins/bootstrap-3.4.1-dist/js/bootstrap.js'>");
        out.println("</script>");
        out.println("</body>");
        out.println("</html>");


    }
}
