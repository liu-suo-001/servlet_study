package aistar.prj.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

@WebServlet("/user/loginView")
public class LoginView extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        Cookie[] cookies = req.getCookies();
        String cookiesName = "username";
        String cookiesPassword = "password";
        String username = "";
        String password = "";

        if(null!=cookies && cookies.length>0){
            for (int i = 0; i < cookies.length; i++) {
                if(cookiesName.equals(cookies[i].getName())){
                    username = cookies[i].getValue();
                    username = URLDecoder.decode(username,"utf-8");
                    username = URLDecoder.decode(username,"utf-8");
                }
                if(cookiesPassword.equals(cookies[i].getName())){
                    password = cookies[i].getValue();
                    password = URLDecoder.decode(password,"utf-8");
                    password = URLDecoder.decode(password,"utf-8");
                }
            }
        }

        String status = req.getParameter("error");
        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
        out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
        out.println("<title>bootstrap是一款响应式的UI[User-Interface]框架</title>");
        out.println("<!-- 引入bootstrap.css - 开发环境/bootstrap.min.css - 生产环境-->");
        out.println("<link rel='stylesheet' href='/j01/plugins/bootstrap-3.4.1-dist/css/bootstrap.css'>");
        out.println("<link rel='stylesheet' href='/j01/css/user/login.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<div class='container'>");
        out.println("<div class='row login'>");
        //TODO... action
        out.println("<form class='form-horizontal' role='form' action='/j01/user/compare' method='get'>");
        out.println("<fieldset>");
        out.println("<legend>");out.println("<h3>用户登录入口");
        out.println("</h3>");out.println("</legend>");

        out.println("<div class='form-group'>");
        out.println("<label for='firstname' class='col-sm-2 control-label'>用户名");out.println("</label>");
        out.println("<div class='col-md-6'>");
        //TODO... 根据控件的name属性的值...
        out.println("<input type='text' class='form-control' id='username' placeholder='请输入用户名' name='username' value='"+username+"'>");
        out.println("</div>");
        if("0".equals(status)){
            out.println("<span style='color:red;font-size:13px;font-weight:900;line-height:33px;margin-left:30px;'>用户名不存在!</span>");
        }
            out.println("</div>");
            out.println("<div class='form-group'>");
            out.println("<label for='lastname' class='col-sm-2 control-label'>密码");
            out.println("</label>");
            out.println("<div class='col-md-6'>");
            //TODO...
            out.println("<input type='password' class='form-control' id='lastname' placeholder='请输入密码' name='password' value='"+password+"'>");
            out.println("</div>");
        if("1".equals(status)){
            out.println("<span style='color:red;font-size:13px;font-weight:900;line-height:33px;margin-left:30px;'>密码错误!</span>");
        }
            out.println("</div>");
            out.println("<div class='form-group'>");
            out.println("<div class='col-sm-offset-2 col-sm-10'>");
            out.println("<div class='checkbox'>");
            out.println("<label>");
            out.println("<input type='checkbox' name='checkbox'>请记住我");
            out.println("</label>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("<div class='form-group'>");
            out.println("<div class='col-sm-offset-2 col-sm-10'>");
            //TODO... submit按钮
            out.println("<button type='submit' class='btn btn-default'>登录");
            out.println("</button>");
            out.println("</div>");
            out.println("</div>");
            out.println("</fieldset>");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");

            out.println("<!-- 引入jquery框架 -->");
            out.println("<script src='/j01/plugins/jquery.min.js'>");
            out.println("</script>");

            out.println("<!-- 引入bootstrap.js/bootstrap.min.js,bootstrap依赖于jquery框架 -->");
            out.println("<script src='/j01/plugins/bootstrap-3.4.1-dist/js/bootstrap.js'>");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
    }
}
