package aistar.prj.controller;

import aistar.prj.model.pojo.Video;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/insert/view")
public class InsertView extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Video video = (Video) req.getAttribute("Video");
        String title = "";
        String likes = "";
        String score = "";
        String videoUrl ="";
        if(null!=video){
            title = video.getTitle();
            likes = String.valueOf(video.getLikes());
            score = String.valueOf(video.getScore());
            videoUrl = video.getVideoUrl();
        }



        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
        out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
        out.println("<title>bootstrap是一款响应式的UI[User-Interface]框架</title>");
        out.println("<!-- 引入bootstrap.css - 开发环境/bootstrap.min.css - 生产环境-->");
        out.println("<link rel='stylesheet' href='/j01/plugins/bootstrap-3.4.1-dist/css/bootstrap.css'>");
        out.println("<link rel='stylesheet' href='/j01/css/user/login.css'>");
        out.println("</head>");
        out.println("<body>");


        out.println("<form class='form-horizontal' role='form' action='/j01/insert/controller'>");
        out.println("<div class='form-group'>");
        out.println("<label class='col-sm-2 control-label'>标题");out.println("</label>");
        out.println("<div class='col-sm-10'>");
        out.println("<input class='form-control' id='focusedInput' type='text' name='title' value='"+title+"' placeholder='请输入标题...'>");
        out.println("</div>");
        out.println("</div>");

        out.println("<div class='col-sm-12' style='display:flex;padding-left:250px'>");
        out.println("<div style='height:300px;'>");
        String p1 = "p1.jpeg";
        String p2 = "p2.jpeg";
        String p3 = "p3.jpeg";
        String p4 = "p4.jpeg";
        String p5 = "p5.jpeg";
        String p6 = "p6.jpeg";

        out.println("<img style='height: 270px;' src='/j01/imgs/p1.jpeg'>"); out.println("<input type='radio'  "+(p1.equals(videoUrl)?"checked":"")+" value='p1.jpeg' name='checkImg' id=''>");
        out.println("</div>");
        out.println("<div style=';height:300px;'>");
        out.println("<img style='height: 270px;' src='/j01/imgs/p2.jpeg'>"); out.println("<input type='radio' "+(p2.equals(videoUrl)?"checked":"")+" value='p2.jpeg' name='checkImg' id=''>");
        out.println("</div>");
        out.println("<div style='height:300px;'>");
        out.println("<img style='height: 270px;' src='/j01/imgs/p3.jpeg'>"); out.println("<input type='radio' "+(p3.equals(videoUrl)?"checked":"")+" value='p3.jpeg' name='checkImg' id=''>");
        out.println("</div>");
        out.println("<div style='height:300px;'>");
        out.println("<img style='height: 270px;' src='/j01/imgs/p4.jpeg'>"); out.println("<input type='radio' "+(p4.equals(videoUrl)?"checked":"")+" value='p4.jpeg' name='checkImg' id=''>");
        out.println("</div>");
        out.println("<div style='height:300px;'>");
        out.println("<img style='height: 270px;' src='/j01/imgs/p5.jpeg'>"); out.println("<input type='radio' "+(p5.equals(videoUrl)?"checked":"")+" value='p5.jpeg' name='checkImg' id=''>");
        out.println("</div>");
        out.println("<div style='height:300px;'>");
        out.println("<img style='height: 270px;' src='/j01/imgs/p6.jpeg'>"); out.println("<input type='radio' "+(p6.equals(videoUrl)?"checked":"")+" value='p6.jpeg' name='checkImg' id=''>");
        out.println("</div>");

        out.println("</div>");

        out.println("<div class='form-group'>");
        out.println("<label class='col-sm-2 control-label'>点赞数");out.println("</label>");
        out.println("<div class='col-sm-10'>");
        out.println("<input class='form-control' id='focusedInput' type='text' name='likes' value='"+likes+"' placeholder='请输入点赞数...'>");
        out.println("</div>");
        out.println("</div>");
        out.println("<div class='form-group'>");
        out.println("<label class='col-sm-2 control-label'>评分");out.println("</label>");
        out.println("<div class='col-sm-10'>");
        out.println("<input class='form-control' id='focusedInput' type='text'  name='score' value='"+score+"' placeholder='请输入评分...'>");
        out.println("</div>");
        out.println("</div>");
        out.println("<div class='form-group'>");
        out.println("<label class='col-sm-2 control-label'>发布");out.println("</label>");
        out.println("<div class='col-sm-10'>");
        out.println("<input style='margin-top: 10px;' checked type='radio' name='putTo' value='0'>");
        out.println("</div>");
        out.println("</div>");
        out.println("<div class='form-group'>");
        out.println("<label class='col-sm-2 control-label'>存为草稿");out.println("</label>");
        out.println("<div class='col-sm-10'>");
        out.println("<input style='margin-top: 10px;' type='radio' name='putTo' value='2'>");
        out.println("</div>");
        out.println("</div>");
        if(null!=video){
            out.println("<div>");
            out.println("<input type='text' value='"+video.getId()+"' name='videoVid' style='display:none'>");
            out.println("<input type='submit' value ='更新' class='btn btn-info' style='width:50px;display: block;margin: auto;'>");
            out.println("</div>");
        }else{
            out.println("<div>");
            out.println("<input type='submit' value='确定' class='btn btn-success' style='width:50px;display: block;margin: auto;'>");
            out.println("</div>");
        }


        out.println("</form>");





        out.println("<!-- 引入jquery框架 -->");
        out.println("<script src='/j01/plugins/jquery.min.js'>");
        out.println("</script>");
        out.println("<!-- 引入bootstrap.js/bootstrap.min.js,bootstrap依赖于jquery框架 -->");
        out.println("<script src='/j01/plugins/bootstrap-3.4.1-dist/js/bootstrap.js'>");
        out.println("</script>");
        out.println("</body>");
        out.println("</html>");
    }
}
