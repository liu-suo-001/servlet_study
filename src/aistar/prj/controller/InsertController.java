package aistar.prj.controller;

import aistar.prj.model.pojo.User;
import aistar.prj.model.pojo.Video;
import aistar.prj.service.IVideoService;
import aistar.prj.service.impl.VideoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(urlPatterns = "/insert/controller")
public class InsertController extends HttpServlet {
    private IVideoService iVideoService = new VideoServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String img = req.getParameter("checkImg");
        String score = req.getParameter("score");
        String likes = req.getParameter("likes");
        String title = req.getParameter("title");
        String status = req.getParameter("putTo");

        User user = (User) req.getSession().getAttribute("user");
        int uid = user.getId();

        String update = req.getParameter("videoVid");
        if(null!=update){

            iVideoService.updateVideo(title,img,Integer.parseInt(likes),Double.parseDouble(score),Integer.parseInt(status),Integer.parseInt(update));
            resp.sendRedirect("/j01/myVideo/personalView");
        }else {
            Video video = new Video();
            video.setTitle(title);
            video.setStatus(Integer.parseInt(status));
            video.setLikes(100);
            video.setUserId(uid);
            video.setScore(Double.parseDouble(score));
            video.setLikes(Integer.parseInt(likes));
            video.setVideoUrl(img);
            video.setCreateDate(new Date());
            int num = iVideoService.insertInto(video);
            System.out.println(num);

            resp.sendRedirect("/j01/myVideo/personalView");
        }
    }
}
