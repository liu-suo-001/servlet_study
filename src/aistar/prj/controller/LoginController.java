package aistar.prj.controller;

import aistar.prj.model.pojo.User;
import aistar.prj.service.IUserService;
import aistar.prj.service.IVideoService;
import aistar.prj.service.impl.UserServiceImpl;
import aistar.prj.service.impl.VideoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/user/*")
public class LoginController extends HttpServlet {
    private IUserService iUserService = new UserServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        switch (pathInfo){
            case "/exit":
                req.getSession().invalidate();
                resp.sendRedirect("/j01/nav/loginView");
                break;

            case "/compare":
                req.setCharacterEncoding("utf-8");
                String username = req.getParameter("username");
                String password = req.getParameter("password");
                String checked = req.getParameter("checkbox");
                System.out.println(checked);


                User u = iUserService.login(username);
                if(u == null){
                    resp.sendRedirect("/j01/user/loginView?error=0");
                    //System.out.println("用户不存在");
                }else if(!password.equals(u.getPassword())){
                    resp.sendRedirect("/j01/user/loginView?error=1");
                    //System.out.println("密码错误!");
                }else{
                    if(checked!=null) {
                        username = URLEncoder.encode(username, "utf-8");
                        username = URLEncoder.encode(username, "utf-8");

                        password = URLEncoder.encode(password, "utf-8");
                        password = URLEncoder.encode(password, "utf-8");

                        Cookie cookie = new Cookie("username",username);
                        cookie.setPath("/");
                        cookie.setMaxAge(60 * 10);
                        Cookie cookie1 = new Cookie("password",password);
                        cookie1.setPath("/");
                        cookie1.setMaxAge(60 * 10);
                        resp.addCookie(cookie);
                        resp.addCookie(cookie1);

                    }
                    HttpSession session = req.getSession();
                    User user = iUserService.login(username);
                    session.setAttribute("user",user);
                    resp.sendRedirect("/j01/video/index");
                }
                break;
        }


    }
}
