package aistar.prj.controller;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.dto.VideoDTO;
import aistar.prj.model.pojo.User;
import aistar.prj.model.pojo.Video;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/video/indexView")
public class IndexSelectView extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = (User) req.getSession().getAttribute("user");

        List<SelectDTO> selects = (List<SelectDTO>) req.getAttribute("list");
        String username = String.valueOf(req.getAttribute("username"));
        String title = String.valueOf(req.getAttribute("title"));
        String likes = String.valueOf(req.getAttribute("likes"));
        String score = String.valueOf(req.getAttribute("score"));


        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
        out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
        out.println("<title>bootstrap是一款响应式的UI[User-Interface]框架</title>");
        out.println("<!-- 引入bootstrap.css - 开发环境/bootstrap.min.css - 生产环境-->");
        out.println("<link rel='stylesheet' href='/j01/plugins/bootstrap-3.4.1-dist/css/bootstrap.css'>");
        out.println("<link rel='stylesheet' href='/j01/css/user/login.css'>");
        out.println("</head>");
        out.println("<body>");
        out.println("<input type='text' name='titleDis' value='"+title+"' style='display: none;'>");
        out.println("<input type='text' name='usernameDis' value='"+username+"' style='display: none; '>");
        out.println("<input type='text' name='scoreDis' value='"+score+"' style='display: none;'>");
        out.println("<input type='text' name='likesDis' value='"+likes+"' style='display: none;'>");


        out.println("<div>");
        out.println("<div class='row' style='margin-top:20px'>");
        //可以在最外层添加一个form表单 - 不是为了做提交的
        out.println("<form id='login_form'>");
        out.println("<div class='col-md-2'>");

        out.println("<input type='text' value='"+title+"' name='title' class='form-control' placeholder='请输入视频标题'>");
        out.println("</div>");
        out.println("<div class='col-md-2'>");

        //String username = (String) req.getAttribute("username");

        out.println("<input type='text' value='"+username+"' name='username' class='form-control' placeholder='请输入主播名称'>");
        out.println("</div>");
        out.println("<div class='col-md-2'>");

        // String likes = (String) req.getAttribute("likes");

        out.println("<input type='text' value='"+likes+"' name='likes' class='form-control' placeholder='请输入点赞量'>");
        out.println("</div>");
        out.println("<div class='col-md-2'>");

        //String score = (String) req.getAttribute("score");

        out.println("<input type='number' value='"+score+"' step='0.5' max='10' min='0' name='score' class='form-control' placeholder='请输入视频评分'>");
        out.println("</div>");
        out.println("<div class='col-md-2'>");
        out.println("<input type='button' id='clickMe' class='btn btn-info' value='搜索'>");
        out.println("</div>");
        out.println("</form>");
        out.println("</div>");
        out.println("</div>");




        out.println("<table class='table'>");
        out.println("<caption>");out.println("</caption>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th><input type='checkbox'></th>");
        out.println("<th>视频编号");out.println("</th>");
        out.println("<th>视频标题");out.println("</th>");
        out.println("<th>视频地址");out.println("</th>");
        out.println(" <th>点赞数&nbsp;&nbsp;<span class='text-warning glyphicon glyphicon-sort-by-attributes'></span>&nbsp;&nbsp;<span class='text-info glyphicon glyphicon-sort-by-attributes-alt'></span>");out.println("</th>");
        out.println("<th>得分");out.println("</th>");
        out.println("<th>作者");out.println("</th>");
        out.println("<th>操作");out.println("</th>");
        out.println("</tr>");
        out.println("</thead>");
        out.println("<tbody>");
        int x = 1;
            for (int i = 0; i < selects.size();i++){
                String name = selects.get(i).getUsername();
                out.println("<tr>");
                out.println("<td><input type='checkbox'></td>");
                out.println("<td>"+(x++)+"</td>");
                out.println("<td>"+selects.get(i).getTitle());out.println("</td>");
                out.println("<td><img style='width:100px;height:100px;' src='/j01/imgs/"+selects.get(i).getVideoUrl()+"'></td>");
                out.println("<td>"+selects.get(i).getLikes());out.println("</td>");
                out.println("<td>"+selects.get(i).getScore());out.println("</td>");
                out.println("<td>"+name);out.println("</td>");
                if(user.getUsername().equals("admin")){
                    out.println("<td style='display:none;'>"+selects.get(i).getId());out.println("</td>");
                    out.println("<td><button type='button' class='btn btn-danger del' >删除</button></td>");
                }else{
                    out.println("<td>");out.println("</td>");
                }
                out.println("</tr>");
            }


        out.println("</tbody>");
        out.println("</table>");



        out.println("<!-- 引入jquery框架 -->");
        out.println("<script src='/j01/plugins/jquery.min.js'>");
        out.println("</script>");
        out.println("<!-- 引入bootstrap.js/bootstrap.min.js,bootstrap依赖于jquery框架 -->");
        out.println("<script src='/j01/plugins/bootstrap-3.4.1-dist/js/bootstrap.js'>");
        out.println("</script>");
        out.println("<script>let clickMe = document.getElementById('clickMe');\n" +
                "clickMe.onclick = function(){\n" +
                "    let title = document.getElementsByName('title')[0].value;\n" +
                "    let likes = document.getElementsByName('likes')[0].value;\n" +
                "    let score = document.getElementsByName('score')[0].value;\n" +
                "    let username = document.getElementsByName('username')[0].value;\n" +
                "    window.location.href='/j01/video/index?title='+title+'&likes='+likes+'&score='+score+'&username='+username;\n" +
                "}</script>");
        out.println("<script>" +
                "let title = document.getElementsByName('titleDis')[0].value;\n" +
                "            let username = document.getElementsByName('usernameDis')[0].value;\n" +
                "            let score = document.getElementsByName('scoreDis')[0].value;\n" +
                "            let likes = document.getElementsByName('likesDis')[0].value;\n" +
                "let d = document.getElementsByClassName('glyphicon-sort-by-attributes')[0];\n" +
                "        d.onclick = function(){\n" +
                "            let sort = 'asc';\n" +
                "            window.location.href='/j01/video/index?title='+title+'&likes='+likes+'&score='+score+'&username='+username+'&sort='+sort;\n" +
                "        }\n" +
                "        let f = document.getElementsByClassName('glyphicon-sort-by-attributes-alt')[0];\n" +
                "        f.onclick = function(){\n" +
                "            let sort = 'desc';\n" +
                "            window.location.href='/j01/video/index?title='+title+'&likes='+likes+'&score='+score+'&username='+username+'&sort='+sort;\n" +
                "        }</script>");

        out.println("<script>$(function(){\n" +
                "            $('.del').click(function(){\n" +
                "                let  y = $(this).parent().prev().text();\n" +
                "    \n" +
                "                window.location.href='/j01/video/index?videoId='+y;\n" +
                "                \n" +
                "            \n" +
                "               \n" +
                "            })\n" +
                "        })</script>");
        out.println("</body>");
        out.println("</html>");

    }
}
