package aistar.prj.controller;

import aistar.prj.model.pojo.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/nav/*")
public class NavViewController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");


        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
        out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
        out.println("<title>bootstrap是一款响应式的UI[User-Interface]框架");out.println("</title>");
        out.println("<!-- 引入bootstrap.css - 开发环境/bootstrap.min.css - 生产环境-->");
        out.println("<link rel='stylesheet' href='/j01/plugins/bootstrap-3.4.1-dist/css/bootstrap.css'>");
        out.println("</head>");
        out.println("<body>");
        out.println("<nav class='navbar navbar-inverse' role='navigation' style='margin-bottom:20px'>");
        out.println("<div class='container-fluid'>");
        out.println("<div class='navbar-header'>");
        out.println("<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#example-navbar-collapse'>");
        out.println("<span class='sr-only'>切换导航");out.println("</span>");
        out.println("<span class='icon-bar'>");out.println("</span>");
        out.println("<span class='icon-bar'>");out.println("</span>");
        out.println("<span class='icon-bar'>");out.println("</span>");
        out.println("</button>");
        out.println("<a class='navbar-brand' href='#'>抖音视频");out.println("</a>");
        out.println("</div>");
        out.println("<div class='collapse navbar-collapse' id='example-navbar-collapse'>");
        out.println("<ul class='nav navbar-nav navbar-right'>");
        out.println("<li class='active'>");out.println("<a href='/j01/video/index'>首页");out.println("</a>");out.println("</li>");
        if(user!=null) {
            out.println("<li>");
            out.println("<a href='#'>"+user.getUsername()+"</a>");
            out.println("</li>");

            out.println("<li>");
            out.println("<a href='/j01/user/exit'>安全退出</a>");
            out.println("</li>");
            out.println("<li>");
            out.println("<a href='/j01/nav/insert'>新增发布</a>");
            out.println("</li>");
        }
        out.println("<li>");out.println("<a href='/j01/nav/loginView'>登录");out.println("</a>");out.println("</li>");
        out.println("<li>");out.println("<a href='#'>注册");out.println("</a>");out.println("</li>");
        out.println("<li>");out.println("<a href='/j01/myVideo/personalView'>我的发布");out.println("</a>");out.println("</li>");
        out.println("<li>");out.println("<a href='#'>");out.println("<span class='glyphicon glyphicon-shopping-cart'>");out.println("</span>&nbsp;我的购物车");out.println("</a>");out.println("</li>");
        out.println("</ul>");
        out.println("</div>");
        out.println("</div>");
        out.println("</nav>");

        out.println("<!-- 引入jquery框架 -->");
        out.println("<script src='/j01/plugins/jquery.min.js'>");out.println("</script>");

        out.println("<!-- 引入bootstrap.js/bootstrap.min.js,bootstrap依赖于jquery框架 -->");
        out.println("<script src='/j01/plugins/bootstrap-3.4.1-dist/js/bootstrap.js'>");out.println("</script>");
        out.println("</body>");
        out.println("</html>");

        String pathInfo = req.getPathInfo();
        switch(pathInfo){
            case "/loginView":
                req.getRequestDispatcher("/user/loginView").include(req,resp);
                break;
            case "/videoView":
                req.getRequestDispatcher("/video/indexView").include(req,resp);
                break;
            case "/personalView":
                req.getRequestDispatcher("/video/my/view").include(req,resp);
                break;
            case "/insert":
                req.getRequestDispatcher("/insert/view").include(req,resp);
                break;
            case "/update":
                req.getRequestDispatcher("/insert/view").include(req,resp);
                break;
        }
    }
}
