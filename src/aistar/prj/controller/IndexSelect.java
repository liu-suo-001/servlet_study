package aistar.prj.controller;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.service.IVideoService;
import aistar.prj.service.impl.VideoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@WebServlet(urlPatterns = "/video/index")
public class IndexSelect extends HttpServlet {
    private IVideoService iVideoService = new VideoServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String vid = req.getParameter("videoId");
        if(null!=vid){
            iVideoService.updateStatusDel(Integer.parseInt(vid));
        }
                String title = req.getParameter("title");
                String likes = req.getParameter("likes");
                String sort = req.getParameter("sort");
                int likes1 = 0;double score1 = 0.0;
                if(null!=likes && likes.trim().length()>0){
                    likes1 = Integer.parseInt(likes);
                }
                String score = req.getParameter("score");
                if(null!=score && score.trim().length()>0){
                    score1 = Double.parseDouble(score);
                }
                String username = req.getParameter("username");

                List<SelectDTO> list;
                if("asc".equals(sort)){
                    list = iVideoService.sortByLikesAsc(title,likes1,score1,username);
                }else if("desc".equals(sort)){
                    list = iVideoService.sortByLikesDesc(title,likes1,score1,username);
                }else{
                    list = iVideoService.findSelect(title,likes1,score1,username);
                }
                req.setAttribute("list",list);
                req.setAttribute("title",title==null?"":title.trim());
                req.setAttribute("username",username==null?"":username.trim());
                req.setAttribute("score",score==null?"":score.trim());
                req.setAttribute("likes",likes==null?"":likes.trim());

                req.getRequestDispatcher("/nav/videoView").forward(req,resp);


    }
}
