package aistar.prj.controller;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.pojo.User;
import aistar.prj.model.pojo.Video;
import aistar.prj.service.IVideoService;
import aistar.prj.service.impl.VideoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/myVideo/*")
public class MyReleaseController extends HttpServlet {
    private IVideoService  iVideoService = new VideoServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        switch (pathInfo){
            case "/personalView":
                String vid = req.getParameter("videoId");
                if(null!=vid){
                    iVideoService.updateStatusDel(Integer.parseInt(vid));
                }
                String vidAdd = req.getParameter("videoIdAdd");
                if(null!=vidAdd){
                    iVideoService.updateStatusDel1(Integer.parseInt(vidAdd));
                }

                User user = (User) req.getSession().getAttribute("user");
                List<Video> videos = iVideoService.searchByUser(user.getUsername());

                req.setAttribute("videos",videos);
                req.getRequestDispatcher("/nav/personalView").forward(req,resp);
                break;
            case "/update":
                String videoId = req.getParameter("videoUpdate");
                if(null!=videoId){
                    Video video = iVideoService.selectByVId(Integer.parseInt(videoId));
                    req.setAttribute("Video",video);
                    req.getRequestDispatcher("/nav/update").forward(req,resp);
                }

        }

    }
}
