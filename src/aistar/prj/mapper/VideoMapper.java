package aistar.prj.mapper;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.dto.VideoDTO;
import aistar.prj.model.pojo.Video;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface VideoMapper {
    @Select("select * from jdbc_video where title=#{value}")
    Video findvideo(String videoName);

    @Select("select * from jdbc_video")
    List<Video>findAll();

    List<Video> findLikes(int likes);
    List<Video> findScore(double score);
    List<Video> findTitle(String title);

    List<SelectDTO> findAllSelect(SelectDTO video);
    List<Video> findByUser(String username);
    int insertVideo(Video video);
    @Update("update jdbc_video set status=1 where id = #{value}")
    int updateStatus(int id);
    @Update("update jdbc_video set status=0 where id = #{value}")
    int updateStatus1(int id);

    @Select("select * from jdbc_video where id=#{value}")
    Video selectById(int id);

    @Update("update jdbc_video set title=#{title},video_url=#{videoUrl},likes=#{likes},score=#{score},status=#{status} where id=#{vid}")
    int updateVideo(@Param("title") String title, @Param("videoUrl") String videoUrl,@Param("likes") int likes,@Param("score") double score,@Param("status") int status,@Param("vid") int vid);
}
