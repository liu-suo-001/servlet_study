package aistar.prj.mapper;


import aistar.prj.model.pojo.User;
import org.apache.ibatis.annotations.Select;


public interface UserMapper {
    @Select("select * from jdbc_user where username=#{value}")
    User findByUsername(String username);

    @Select("select * from jdbc_user where id = #{value}")
    User findById(int id);

    @Select("select * from jdbc_user where username like concat('%',#{value},'%')")
    User findUserNameLike(String username);
}
