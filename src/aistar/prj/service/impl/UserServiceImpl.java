package aistar.prj.service.impl;

import aistar.prj.mapper.UserMapper;
import aistar.prj.model.pojo.User;
import aistar.prj.service.IUserService;
import aistar.prj.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.sql.SQLException;

public class UserServiceImpl implements IUserService {
    @Override
    public User login(String username) {
        User u = null;
        SqlSession sqlSession = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            UserMapper userMapper =sqlSession.getMapper(UserMapper.class);
            u = userMapper.findByUsername(username);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return u;
    }
}
