package aistar.prj.service.impl;

import aistar.prj.mapper.UserMapper;
import aistar.prj.mapper.VideoMapper;
import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.dto.VideoDTO;
import aistar.prj.model.pojo.Video;

import aistar.prj.service.IVideoService;
import aistar.prj.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class VideoServiceImpl implements IVideoService {

    @Override
    public List<VideoDTO> findALL() {
        List<VideoDTO> list = new ArrayList<>();
        SqlSession sqlSession = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            VideoMapper videoMapper = sqlSession.getMapper(VideoMapper.class);
            List<Video> videos = videoMapper.findAll();
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            for (int i = 0; i < videos.size(); i++) {
                VideoDTO dto = new VideoDTO();
                dto.setVideo(videos.get(i));
                int uid = videos.get(i).getUserId();
                dto.setUsername(userMapper.findById(uid).getUsername());
                list.add(dto);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    @Override
    public List<SelectDTO> findSelect(String title,int likes,double score,String username) {
        List<SelectDTO> list = new ArrayList<>();
        SqlSession sqlSession = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            VideoMapper videoMapper = sqlSession.getMapper(VideoMapper.class);
            SelectDTO selectDTO = new SelectDTO();
            selectDTO.setTitle(title);
            selectDTO.setScore(score);
            selectDTO.setLikes(likes);
            selectDTO.setUsername(username);
            list = videoMapper.findAllSelect(selectDTO);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
       return list;
    }

    @Override
    public List<SelectDTO> sortByLikesDesc(String title,int likes,double score,String username) {
        List<SelectDTO> list = new ArrayList<>();
        List<SelectDTO> list1 = new ArrayList<>();
        SqlSession sqlSession = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            VideoMapper videoMapper = sqlSession.getMapper(VideoMapper.class);
            SelectDTO selectDTO = new SelectDTO();
            selectDTO.setTitle(title);
            selectDTO.setScore(score);
            selectDTO.setLikes(likes);
            selectDTO.setUsername(username);
            list = videoMapper.findAllSelect(selectDTO);
            list.sort((o1,o2)-> o2.getLikes()-o1.getLikes());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public List<SelectDTO> sortByLikesAsc(String title, int likes, double score, String username) {
        List<SelectDTO> list = new ArrayList<>();
        List<SelectDTO> list1 = new ArrayList<>();
        SqlSession sqlSession = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            VideoMapper videoMapper = sqlSession.getMapper(VideoMapper.class);
            SelectDTO selectDTO = new SelectDTO();
            selectDTO.setTitle(title);
            selectDTO.setScore(score);
            selectDTO.setLikes(likes);
            selectDTO.setUsername(username);
            list = videoMapper.findAllSelect(selectDTO);
            list.sort(Comparator.comparingInt(SelectDTO::getLikes));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }
    @Override
    public List<Video> searchByUser(String username) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        List<Video> list = null;
        List<Video> listStatus2 = new ArrayList<>();
        List<Video> listStatusNot2 = new ArrayList<>();
        List<Video> list1 = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
            list = videoMapper.findByUser(username);
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i).getStatus()==2){
                    listStatus2.add(list.get(i));
                }else{
                    listStatusNot2.add(list.get(i));
                }
            }
            listStatus2.sort(((o1, o2) -> Math.toIntExact(o2.getCreateDate().getTime() - o1.getCreateDate().getTime())));
            listStatusNot2.sort(((o1, o2) -> Math.toIntExact(o2.getCreateDate().getTime() - o1.getCreateDate().getTime())));
            list1 = listStatus2;
            list1.addAll(listStatusNot2);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return list1;
    }

    @Override
    public int insertInto(Video video) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        int num = 0;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);

            num = videoMapper.insertVideo(video);
            sqlSession.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return num;
    }

    @Override
    public int updateStatusDel(int id) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        int num = 0;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
            num = videoMapper.updateStatus(id);
            sqlSession.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return num;
    }

    @Override
    public int updateStatusDel1(int id) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        int num = 0;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
            num = videoMapper.updateStatus1(id);
            sqlSession.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return num;
    }

    @Override
    public Video selectByVId(int id) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        Video video = null;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
            video = videoMapper.selectById(id);
            sqlSession.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return video;
    }

    @Override
    public int updateVideo(String title, String videoUrl, int likes, double score, int status, int vid) {
        SqlSession sqlSession = null;
        VideoMapper videoMapper = null;
        int num = 0;
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
            num = videoMapper.updateVideo(title,videoUrl,likes,score,status,vid);
            sqlSession.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            MybatisUtil.close(sqlSession);
        }
        return num;
    }

}
