package aistar.prj.service;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.dto.VideoDTO;
import aistar.prj.model.pojo.User;
import aistar.prj.model.pojo.Video;

import java.util.List;

public interface IVideoService {

    List<VideoDTO> findALL();

    List<SelectDTO> findSelect(String title,int likes,double score,String username);

    List<SelectDTO> sortByLikesDesc(String title,int likes,double score,String username);

    List<SelectDTO> sortByLikesAsc(String title,int likes,double score,String username);
    List<Video> searchByUser(String username);

    int insertInto(Video video);
    int updateStatusDel(int id);
    int updateStatusDel1(int id);
    Video selectByVId(int id);
    int updateVideo(String title,String videoUrl,int likes,double score,int status,int vid);
}
