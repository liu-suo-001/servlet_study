package aistar.prj.service;

import aistar.prj.model.pojo.User;

public interface IUserService {
    User login(String username);

}
