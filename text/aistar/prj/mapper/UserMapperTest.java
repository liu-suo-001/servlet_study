package aistar.prj.mapper;

import aistar.prj.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;


public class UserMapperTest {
    private UserMapper userMapper;
    private SqlSession sqlSession;

    @Before
    public void testBefore(){
        try {
            sqlSession = MybatisUtil.getSqlSession();
            userMapper = sqlSession.getMapper(UserMapper.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @Test
    public void testSelectVideo(){
        System.out.println(userMapper.findByUsername("admin"));
    }

    @Test
    public void testUserNameLike(){
        System.out.println(userMapper.findUserNameLike("wu"));
    }
    @After
    public void testAfter(){
        MybatisUtil.close(sqlSession);
    }
}
