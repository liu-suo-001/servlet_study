package aistar.prj.mapper;

import aistar.prj.model.dto.SelectDTO;
import aistar.prj.model.dto.VideoDTO;
import aistar.prj.model.pojo.Video;
import aistar.prj.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Date;

public class VideoMapperTest {
    private SqlSession sqlSession;
    private VideoMapper videoMapper;
    @Before
    public void testBefore(){
        try {
            sqlSession = MybatisUtil.getSqlSession();
            videoMapper = sqlSession.getMapper(VideoMapper.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @After
    public void testAfter(){
        sqlSession.commit();
        MybatisUtil.close(sqlSession);
    }
    @Test
    public void testLikes(){
        System.out.println(videoMapper.findLikes(200));
    }

    @Test
    public void testScore(){
        System.out.println(videoMapper.findScore(4.5));
    }

    @Test
    public void testTitle(){
        System.out.println(videoMapper.findTitle("dd"));
    }

    @Test
    public void testSelectAll(){
        SelectDTO videoDTO = new SelectDTO();
        videoDTO.setUsername("admin");
        Video video = new Video();
        video.setTitle("w");
        videoDTO.setLikes(100);
        videoDTO.setScore(2.0);
        System.out.println(videoMapper.findAllSelect(videoDTO));
    }

    @Test
    public void testInsertVideo(){
        Video video = new Video();
        video.setVideoUrl("p1.jpeg");
        video.setCreateDate(new Date());
        video.setLikes(200);
        video.setScore(3.9);
        video.setUserId(1);
        video.setStatus(1);
        video.setTitle("dasda");
        int x = videoMapper.insertVideo(video);
        System.out.println(x);
        sqlSession.commit();

    }

    @Test
    public void testUpdate(){
        System.out.println(videoMapper.updateStatus(4));
    }
    @Test
    public void testUpdateto(){
        System.out.println(videoMapper.updateVideo("dada", "p3.jpeg", 400, 4.6, 1, 2));
    }
}
